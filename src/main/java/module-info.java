module eleer.idatt2001.cardgame {
    requires javafx.controls;
    requires javafx.fxml;

    exports eleer.idatt2001.cardgame.GUI;
    opens eleer.idatt2001.cardgame.GUI to javafx.fxml;
    exports eleer.idatt2001.cardgame.cards;
    opens eleer.idatt2001.cardgame.cards to javafx.fxml;
}