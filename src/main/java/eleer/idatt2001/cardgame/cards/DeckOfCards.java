package eleer.idatt2001.cardgame.cards;

import java.util.ArrayList;
import java.util.Random;

/**
 * Class representing a deck of cards
 *
 * @author Eirik Leer Talstad
 */
public class DeckOfCards {

    public ArrayList<PlayingCard> deck;
    public final char[] suit = {'S', 'H', 'D', 'C'};

    /**
     * Initializes deck with 52 cards
     */
    public DeckOfCards() {
        deck = new ArrayList<>();
        for(int i = 0; i < 4; i ++) {
            for(int j = 1; j < 14; j++) {
                PlayingCard card = new PlayingCard(suit[i], j);
                deck.add(card);
            }
        }
    }

    /**
     * Deals a hand with specified number of cards, and removes card that are dealt from the deck
     * Throws exception if number of cards drawn is invalid.
     *
     * @param number of cards to deal
     * @return ArrayList with cards that are dealt
     */
    public ArrayList<PlayingCard> dealHand(int number) {
        if(getDeck().size() == 0) throw new IllegalArgumentException("Deck is empty");
        if(number > getDeck().size() || number < 0) throw new IllegalArgumentException("Number of cards drawn cannot be higher than deck size, or negative");

        ArrayList<PlayingCard> pickedCards = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < number; i++) {
            int randomInt = random.nextInt(deck.size());
            PlayingCard card = deck.get(randomInt);
            deck.remove(randomInt);
            if (!pickedCards.contains(card)) {
                pickedCards.add(card);
            }
        }
        return pickedCards;
    }

    /**
     * Gets deck.
     *
     * @return deck of cards
     */
    public ArrayList<PlayingCard> getDeck() {
        return deck;
    }
}
