package eleer.idatt2001.cardgame.cards;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Class representing a hand of cards
 *
 * @author Eirik Leer Talstad
 */
public class CardHand {

    public ArrayList<PlayingCard> hand;

    /**
     * Initializes empty hand of cards
     */
    public CardHand() {
        hand = new ArrayList<>();
    }

    /**
     * Initializes hand of cards with a given ArrayList
     * @param hand
     */
    public CardHand(ArrayList<PlayingCard> hand) {
        this.hand = hand;
    }

    /**
     * Gets card hand
     * @return
     */
    public ArrayList<PlayingCard> getHand() {
        return hand;
    }

    /**
     * Adds card to card hand
     *
     * @param card to be added to the card hand
     * @return
     */
    public boolean addCard(PlayingCard card) {
        if(hand.contains(card)) {
            return false;
        } else {
            return hand.add(card);
        }
    }

    /**
     * Checks if given card is in card hand
     *
     * @param card to be compared to
     * @return true if card is in hand, false if not
     */
    public boolean isCardInHand(PlayingCard card) {
        return hand.stream().anyMatch(n -> n.toString().equals("S12"));
    }

    /**
     * Method that gets the sum of all cards in a hand
     * @return int - sum of all cards
     */
    public int sumOfCards() {
        return hand
                .stream()
                .map(PlayingCard::getFace).reduce(0,Integer::sum);
    }

    /**
     * Method that filters out all cards of a specified suit, and returns them in a new ArrayList.
     * @param suit
     * @return ArrayList - all cards of a specified suit
     */
    public ArrayList<PlayingCard> getAllSuit(char suit) {
        return hand.stream().filter(card -> card.getSuit() == suit).collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Method that checks if there are 5 cards of the same suit in a hand.
     * @return True if flush and false if not flush
     */
    public boolean isFlush() {
        return hand.stream().map(PlayingCard::getSuit).distinct().count() == 1;
    }

    @Override
    public String toString() {
        return hand.toString();
    }
}
