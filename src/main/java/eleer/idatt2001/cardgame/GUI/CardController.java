package eleer.idatt2001.cardgame.GUI;

import eleer.idatt2001.cardgame.cards.CardHand;
import eleer.idatt2001.cardgame.cards.DeckOfCards;
import eleer.idatt2001.cardgame.cards.PlayingCard;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class CardController {

    DeckOfCards deck;
    CardHand cardHand;

    @FXML
    public Label cardsInHand;
    public Label sumOfFaces;
    public Label cardsOfHearts;
    public Label isFlush;
    public Label hasQueenOfSpades;

    @FXML
    public void dealHand(ActionEvent event) {
        deck = new DeckOfCards();
        cardHand = new CardHand(deck.dealHand(5));
        cardsInHand.setText(cardHand.getHand().toString());

    }

    @FXML
    public void checkHand(ActionEvent event) {
        sumOfFaces.setText(String.valueOf(cardHand.sumOfCards()));
        cardsOfHearts.setText(cardHand.getAllSuit('H').toString());
        String flush;
        isFlush.setText(flush = cardHand.isFlush() ? "Yes" : "No");
        String hasCard;
        hasQueenOfSpades.setText(hasCard = cardHand.isCardInHand(new PlayingCard('S', 12)) ? "Yes" : "No");
    }
}
