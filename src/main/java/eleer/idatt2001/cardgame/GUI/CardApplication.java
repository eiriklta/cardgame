package eleer.idatt2001.cardgame.GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class CardApplication extends Application {


    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(CardApplication.class.getClassLoader().getResource("CardGameGUI.fxml"));

        Scene scene = new Scene(fxmlLoader.load(), 600, 450);

        stage.setTitle("Cardgame");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
