package eleer.idatt2001.cardgame.cards;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class DeckOfCardsTest {

    DeckOfCards deck;

    @BeforeEach
    public void initializeDeck() {
        deck = new DeckOfCards();
    }

    @Test
    public void deck_of_cards_has_the_right_amount_of_cards() {
        assertEquals(52, deck.getDeck().size());
    }

    @Test
    public void dealHand_deals_right_amount_of_cards() {
        CardHand hand = new CardHand(deck.dealHand(30));
        assertEquals(30, hand.getHand().size());
    }

    @Test
    public void exception_thrown_when_trying_to_deal_while_deck_is_empty() {
        deck.dealHand(52);
        assertThrows(IllegalArgumentException.class, () -> {
            deck.dealHand(1);
        });
    }

    @ParameterizedTest
    @ValueSource(ints = {53, -1, -10})
    public void exception_thrown_when_number_of_cards_drawn_is_invalid(int number) {
        assertThrows(IllegalArgumentException.class, () -> {
            deck.dealHand(number);
        });
    }
}