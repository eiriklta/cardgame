package eleer.idatt2001.cardgame.cards;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;


@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class PlayingCardTest {

    @ParameterizedTest
    @ValueSource(ints = {14, 0, -1})
    public void card_face_cannot_be_negative_or_higher_than_thirteen(int face) {
            assertThrows(IllegalArgumentException.class, () -> {
                new PlayingCard('S', face);
            });
        }

    @ParameterizedTest
    @ValueSource(chars = {'A', 'X', '.'})
    public void card_suit_must_be_valid(char suit) {
        assertThrows(IllegalArgumentException.class, () -> {
            new PlayingCard(suit, 1);
        });
    }
}

