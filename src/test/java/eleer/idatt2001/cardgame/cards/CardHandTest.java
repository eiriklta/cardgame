package eleer.idatt2001.cardgame.cards;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class CardHandTest {

    @Test
    public void correct_sum_is_returned() {
        CardHand hand = new CardHand();
        hand.addCard(new PlayingCard('S', 10));
        hand.addCard(new PlayingCard('H', 10));
        hand.addCard(new PlayingCard('D' , 10));
        hand.addCard(new PlayingCard('C', 10));

        assertEquals(40, hand.sumOfCards());
    }

    @Nested
    @DisplayName("Tests for checking if isFlush method works as expected")
    public class FlushTests {

        @Test
        public void hand_is_flush_if_all_five_cards_are_same_suit() {
            CardHand hand = new CardHand();
            for(int i = 1; i <= 5; i++) hand.addCard(new PlayingCard('S', i));
            assertTrue(hand.isFlush());
        }

        @Test
        public void hand_is_not_flush_with_four_same_suit() {
            CardHand hand = new CardHand();
            for(int i = 1; i <= 4; i++) hand.addCard(new PlayingCard('S', i));
            hand.addCard(new PlayingCard('C', 5));
            assertFalse(hand.isFlush());
        }
    }
}
